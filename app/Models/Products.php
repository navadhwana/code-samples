<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    
    protected $fillable = ['name', 'sku', 'price', 'status', 'image'];

    /**
     * Model validation rules while saving the data
     */
    public static function validationRules($productId = false) {
        $results = [
            'name' => 'required',
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'sku' => 'required|unique:products',
            'status' => 'required'
        ];
        
        // IN case EDIT product, we need to make sure it is not duplicate sku 
        if($productId) {
            $results['sku'] .= ',sku,'.$productId;
        } 

        return $results;
    }
}

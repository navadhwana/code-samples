@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Product List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
            </div>
        </div>
    </div>
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>SKU</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        @foreach ($products as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->sku }}</td>
            <td>{{ $value->price }}</td>
            <td>
                @if($value->status == '1') 
                <span class="label label-success">Enabled</span>
                @else
                <span class="label label-danger">Disabled</span>
                @endif
            </td>
            <td>
                <form action="{{ route('products.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('products.show',$value->id) }}">Show</a>    
                    <a class="btn btn-primary" href="{{ route('products.edit',$value->id) }}">Edit</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
    {!! $products->links() !!}      
@endsection